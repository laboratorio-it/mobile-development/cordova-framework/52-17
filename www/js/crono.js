//global var
var secondsRemaining;
var intervalHandle;

function resetPage() {
  document.getElementById("inputArea").style.display = "block";
}

function tick() {
  //get h1
  timeDisplay = document.getElementById("time");
  //seconds to m:s
  var min = Math.floor(secondsRemaining / 60);
  var sec = secondsRemaining - (min * 60);
  //leading 0 when <10
  if (sec < 10) {
    sec = "0" + sec;
  }
  var message = min + ":" + sec;
  timeDisplay.innerHTML = message;
  //stop if 0
  if (secondsRemaining === 0) {
    alert("done");
    clearInterval(intervalHandle);
    resetPage();
  }
  secondsRemaining--;
}

function startCountdown() {
  //get input value
  var minutes = document.getElementById("minutes").value;
  //check number
  if (isNaN(minutes)) {
    alert("Please type a number!");
    return;
  }
  //seconds
  secondsRemaining = minutes * 60;
  intervalHandle = setInterval(tick, 1000);
  //hide input
  document.getElementById("inputArea").style.display = "none";
}

window.onload = function() {
  //create text box
  var inputMinutes = document.createElement("input");
  inputMinutes.setAttribute("id", "minutes");
  inputMinutes.setAttribute("type", "text");
  //create button
  var startButton = document.createElement("input");
  startButton.setAttribute("id", "button");
  startButton.setAttribute("type", "button");
  startButton.setAttribute("value", "Start countdown");
  startButton.onclick = function() {
      startCountdown();
    }
    //inset to div
  document.getElementById("inputArea").appendChild(inputMinutes);
  document.getElementById("inputArea").appendChild(startButton);
}